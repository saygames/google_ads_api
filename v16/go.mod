module google.golang.org/genproto/googleapis/ads/googleads/v16

go 1.20

require (
	cloud.google.com/go/longrunning v0.5.5
	google.golang.org/genproto/googleapis/api v0.0.0-20240311173647-c811ad7063a7
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240311173647-c811ad7063a7
	google.golang.org/grpc v1.62.1
	google.golang.org/protobuf v1.33.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
