#!/bin/bash

V=18

set -e
cd "$(dirname "$0")/.."

rm -rf './googleapis'
git clone https://github.com/googleapis/googleapis.git
I_DIR=./googleapis
SRC_DIR="$I_DIR/google/ads/googleads/v$V/*/*.proto"

protoc -I="$I_DIR" --go_out=. --go-grpc_out=. $SRC_DIR

DEST="./v$V"
rm -rf "$DEST"/*
mkdir "$DEST" 2>/dev/null || true
mv "./google.golang.org/genproto/googleapis/ads/googleads/v$V"/* "$DEST"
mv "$DEST/resources/experiment_arm.pb.go" "$DEST/resources/experiment_arm_.pb.go"
rm -rf './google.golang.org'
rm -rf './googleapis'

cd "$DEST"
go mod init "google.golang.org/genproto/googleapis/ads/googleads/v$V"
go mod tidy
