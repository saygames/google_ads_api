module google.golang.org/genproto/googleapis/ads/googleads/v14

go 1.18

require (
	cloud.google.com/go/longrunning v0.5.1
	google.golang.org/genproto/googleapis/api v0.0.0-20230717213848-3f92550aa753
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230717213848-3f92550aa753
	google.golang.org/grpc v1.56.2
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/genproto v0.0.0-20230706204954-ccb25ca9f130 // indirect
)
