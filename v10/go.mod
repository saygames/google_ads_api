module google.golang.org/genproto/googleapis/ads/googleads/v10

go 1.17

require (
	google.golang.org/genproto v0.0.0-20220328150716-24ca77f39d1f
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.28.0
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.5 // indirect
)
