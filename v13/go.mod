module google.golang.org/genproto/googleapis/ads/googleads/v13

go 1.17

require (
	google.golang.org/genproto v0.0.0-20230301171018-9ab4bdc49ad5
	google.golang.org/grpc v1.53.0
	google.golang.org/protobuf v1.28.1
)

require (
	cloud.google.com/go/longrunning v0.4.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
)
